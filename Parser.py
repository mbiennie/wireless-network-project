# Import
import numpy as np
from sk_dsp_comm import fec_conv as fec

# Class de manipuations de bits (de façon très générale)
# La class contient de nombreuses fonctions utiles pour le problème
class Parser:
    # Constructeur de la class
    def __init__(self, manipulator):
        self.signal = manipulator.signal
        self.manipulator = manipulator

    # Modulation hamming748
    def hamming748(self, X):	
        y0 = X[0]
        #print(y0)
        y1= X[1]
        #print(y1)
        y2 = X[2]
        #print(y2)
        y3 = X[3]
        #print(y3)
        y4 = y3 ^ y1 ^ y2
        y5 = y0 ^ y1 ^ y3
        y6 = y0 ^ y1 ^ y2
        y7 = y0 ^ y1 ^ y2 ^ y3 ^ y4 ^ y5 ^ y6
        return np.array([[y0],[y1],[y2],[y3],[y4],[y5],[y6],[y7]])
        
    # décodage de hamming748
    def hamming748_decode(self, encoded):
        # Define the parity check matrix H
        H = [
            [1, 0, 1, 0, 1, 0, 1],
            [0, 1, 1, 0, 0, 1, 1],
            [0, 0, 0, 1, 1, 1, 1]
        ]
        buff = []
        tab = []
        i=0
        decoded = []
        if len(encoded)>7:
            for bit in encoded:
                buff.append(bit)
                if i>=7:
                     tab.append(buff)
                     buff=[]
                     i=-1
                i+=1
        else:
            tab.append(encoded)
        for octect in tab:
            syndrome = [0, 0, 0]
            for i in range(3):
                for j in range(7):
                    syndrome[i] ^= (H[i][j] & octect[j])

            error_position = syndrome[0] * 1 + syndrome[1] * 2 + syndrome[2] * 4
            if error_position != 0:
                #print(f"Error detected at position {error_position}. Correcting...")
                octect[error_position - 1] ^= 1
            decoded.append([octect[0], octect[1], octect[2], octect[3]])
        if len(decoded)>1:
            decoded2 = []
            for d in decoded:
                for bit in d:
                    decoded2.append(bit)
            return decoded2
        
        return decoded[0]

    # Cast du biaire en décimale
    def bin2dec(self, nb):
        n = "0b"
        for b in nb:
            n = n + str(b)
        return int(n, 2)

    # Il y a toutes les informaitons contenues dans le PBSK
    def extract_data_from_PBSK(self, PBSK):
        # On décode le signal qui est encodé via hamming
        B = self.hamming748_decode(self.manipulator.bpsk_demod(PBSK))

        # On extrait le contenu du signal
        dic = {} # Dictionnaire
        cellIdent=[]
        nbUser = []
        j=-1
        for i in B:
            j=j+1
            if j<18:
                cellIdent.append(i)
            else:
                nbUser.append(i)

        dic['cellIdent'] = self.bin2dec(cellIdent) # Identificateur de la cellule PBSK
        dic['nbUser'] = self.bin2dec(nbUser) # affectation du nombre d'user dans le dico
        return dic

    # Cette fonction extrait les données d'une matrice sous la forme d'un seul vecteur "continue"
    # On prend les entrées suivantes
    # - un matrice qui dois être des dimension de la matrice du signal sans la sycro
    # - première cellule a extraire
    # - longeure du vecteur a extraire
    #
    # Finalement on obtiens return_matrix qui est le vecteur continue
    def extract_information(self,matrix,debut,longueur):
        ligne_fin = debut+longueur
        print("La ligne de fin est :",ligne_fin)
        print("La ligne de debut est :",debut)
        ligne = ligne_fin
        return_matrix = []
        coloneDebut = 0
        coloneFin = 0
        g = 0
        if ligne_fin>622:
            g = ligne_fin
            coloneDebut = int(debut/623)
            coloneFin = coloneDebut
            while g>622:
                g-=622
                coloneFin+=1
            ligne =ligne_fin-623
            print("Je commence à debut : ",debut," et je termine à la ligne ", ligne)
        else:
            coloneFin = int(ligne_fin/623)
        print("Je commence à la ligne colone ",coloneDebut," et je termine à la colone : ",coloneFin)
        for c in range(coloneDebut,coloneFin+1):
            if c==coloneDebut and ligne_fin<=622:
                for l in range(debut,ligne):
                    return_matrix.append(matrix[c][l])
            elif c==coloneDebut and ligne_fin>622:
                for l in range(debut,622):
                    return_matrix.append(matrix[c][l])
            elif c==coloneFin:
                for l in range(0,ligne):
                    return_matrix.append(matrix[c][l])
                break
            elif c>coloneDebut:
                for l in range(0,ligne):
                    return_matrix.append(matrix[c][l])
        return return_matrix
    
    # Cette fonction nous donne les informations contenue dans un PBCH d'un utilisateur sous la forme d'un dictionnaire
    # On a les entrées suivantes :
    # - matrix : matrice di signal sans les channel de sycro
    # - number : numéro de l'user dont on veut les information du PBCH
    #
    # Cette fonction renvoi un tableau de dico de tout les pbch
    def addUserDicPBCH(self,matrix,number):
        signal = self.manipulator
        user_info = signal.get_PBSK(matrix)
        dic_users=[]
        #print("Dic user : ",user_info)
        dic_info_users = self.extract_data_from_PBSK(user_info)
        print(dic_info_users)
        cellInfo = m = self.extract_information(matrix,0,48)
        dic_cellInfo = self.extract_data_from_PBSK(user_info)
        for user in range(1,dic_info_users["nbUser"]-1):
            m = self.extract_information(matrix,48*user,48)
            user_info = signal.bpsk_demod(m)
            #print("Cela donne pour user N°",user,":",user_info)
            dic_info_user = self.extract_data_from_PBCHU(user_info)
            if dic_info_user["userIdent"]==number:
                return dic_cellInfo,dic_info_user
            dic_users.append(dic_info_user)
        return dic_cellInfo, dic_users

    def extract_data_from_PBCHU(self,user_info):
        B_user = self.hamming748_decode(user_info)
        print("J'obtiens après transformation : ",B_user," de longueur ",len(user_info))
        cellIdent=[]
        MSC = []
        symb = []
        RB = []
        HARQ = []
        dic_user={}
        j=-1
        for i in B_user:
            j=j+1
            if j<8:
                cellIdent.append(i)
            elif j>=8 and j<10:
                MSC.append(i)
            elif j>=10 and j<14:
                symb.append(i)
            elif j>=14 and j<20:
                RB.append(i)
            elif j>=20 and j<24:
                HARQ.append(i)
    	# On affecte les données du PBCHU dans le dico 
        dic_user['userIdent'] = self.bin2dec(cellIdent)
        dic_user['MSC'] = self.bin2dec(MSC)
        dic_user['symb'] = self.bin2dec(symb)
        dic_user['RB'] = self.bin2dec(RB)
        dic_user['HARQ'] = self.bin2dec(HARQ)
        return dic_user
    
    def addUserDicPDCCHU(self,matrix,MSC,RS,SYMB):
        # Comme on a changé la matrice et enlevé les channel de sycro, on soustrait 2 a symb
        # Comme notre indexage commence a 0, on doit soustraire 2 aux deux valeurs
        RS -= 1
        SYMB -= 3
        ligne_debut = RS*12
        ligne_fin = RS * 12 +72
        g = 0
        coloneDebut=SYMB
        coloneFin=SYMB
        data = []
        ligne=ligne_fin
        if ligne_fin>622:
            g = ligne_fin
            coloneDebut = int(ligne_debut/623)
            coloneFin = coloneDebut
            while g>622:
                g-=622
                coloneFin+=1
                ligne =ligne_fin-623
            print("Je commence à debut : ",ligne_debut," et je termine à la ligne ", ligne)
        for c in range(coloneDebut,coloneFin+1):
            if c==coloneDebut and ligne_fin<=622:
                for l in range(ligne_debut,ligne):
                    data.append(matrix[c][l])
            elif c==coloneDebut and ligne_fin>622:
                for l in range(ligne_debut,622):
                    data.append(matrix[c][l])
            elif c==coloneFin:
                for l in range(0,ligne):
                    data.append(matrix[c][l])
                break
            elif c>coloneDebut:
                for l in range(0,ligne):
                    data.append(matrix[c][l])
        dic_info_user = self.PDCCHU_decode(data,MSC)
        return dic_info_user

    # Décodage de PDCCHU en fonction du MSC
    def PDCCHU_decode(self, PDCCHU,MSC):
        dic_user={}
        X_user = []
        if MSC == 1:
            X_user = self.manipulator.bpsk_demod(PDCCHU)
        if MSC == 2:
            X_user = self.manipulator.qpsk_demod(PDCCHU)
        B_user = self.hamming748_decode(X_user)
        #print("La taille de B_user est : ",B_user)
        cellIdent=[]
        MSC = []
        symb = []
        RB = []
        RBsize = []
        CRC = []
        j=-1
        for i in B_user:
            j=j+1
            if j<7:
                cellIdent.append(i)
            elif j<=13:
                MSC.append(i)
            elif j<=17:
                symb.append(i)
            elif j<24:
                RB.append(i)
            elif j<34:
                RBsize.append(i)
            else:
                CRC.append(i)
        dic_user['userIdent'] = self.bin2dec(cellIdent)
        dic_user['MSC'] = self.bin2dec(MSC)
        dic_user['symb'] = self.bin2dec(symb)
        dic_user['RB'] = self.bin2dec(RB)
        dic_user['RB_size'] = self.bin2dec(RBsize)
        dic_user['CRC'] = self.bin2dec(CRC)
        #print("La taille de CRC est de ",len(CRC))
        return dic_user

    # On décode le PDSCH a l'aide de FEC
    def PDSCH_fec(self, qamSeq, mcs):
        # Dans le premier cas, on prend les valeurs de mcs pour lesquelles on utilise hamming
        if mcs in [25,26,27]:
            return self.hamming748_decode(qamSeq)
        
        # Dans le second cas, on est en code convolutionel et on utilise le code donné dans le sujet
        elif mcs in [5,6,7]:
            cc1 = fec.FECConv((1011011,1111001),Depth=6)
            return cc1.viterbi_decoder(np.array(qamSeq).astype(int),'hard')
        return -1 # en cas d'écheck, on retourne -1
