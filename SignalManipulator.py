# Import
import numpy as np
import math

# Class de manipulation du signal
class SignalManipulator:
    def __init__(self, signal_file):
         # On récupère les données reçus
        my_data = np.genfromtxt('./'+signal_file, delimiter=';')
        self.mat_complex = my_data[:,0::2] +1j*my_data[:,1::2]

        # Attributs de la class
        self.signal = self.__clean_receiv_signal(self.mat_complex) # Signal reçus

# on a 14 symbole et 1024 buchettes
# Traite le tableau correctement pour pouvoir l'afficher
# On met les buchettes correctement
# On récupère donc les 624 bonne fréquences
    def __clean_receiv_signal(self,informations):
        N = len(informations[0])
        Nre = 624
#        buff = 1
        tfMatrix_short_buff=[]
        tfMatrix_short=[]
        for x in range(0,14):
            for y in range(1,int(Nre/2)+1):
                tfMatrix_short_buff.append(informations[x,y])
            for y in range(N-int(Nre/2)+1,N):
                tfMatrix_short_buff.append(informations[x,y])
            tfMatrix_short.append(tfMatrix_short_buff)
            tfMatrix_short_buff = []
        return tfMatrix_short

    # --- Removing PSCH and SSCH channels
    def remove_syncro_channels(self,matrix):
        qamMatrix = []
        # On parcours le tableau a partire de la 3em ligne car les deux première contiennent du PSCH et SSCH
        for i in range(2,14):
            qamMatrix.append(matrix[i])
        return qamMatrix

    # On récupère le PBSK
    def get_PBSK(self,matrix):
        qamMatrix = self.remove_syncro_channels(matrix)
        PBSK = [qamMatrix[0][n] for n in range(0,48)]
        return PBSK

    # On récupère le PBCH du signal
    def get_PBCH(self):
        # dans un premier temps on récupère la matrice sans les channel de sycro
        qamMatrix = self.remove_syncro_channels(self.signal)
        # On attribue le PBCHU que l'on va traiter
        PBCHU = [qamMatrix[0][n] for n in range(48*1,48*2)] 
        return PBCHU

    # Démudulation du signal bpsh en deux partie
    def bpsk_demod(self, matrix):
        buff =[]
        for i in matrix:
            buff.append(int(i.real>=0))
        #print("La valeur de retour : ",hamming748_decode(buff))
        return buff

    # démobulation de PDCCHU selon l'algo qpsk
    def qpsk_demod(self, PDCCH):
        decision = []
        for cons in PDCCH:
            decision.append(int(cons.real>=0))
            decision.append(int(cons.imag>=0))
        return decision

    # Fonction de démodulation de quam16
    def qam16_demod(self, input_sequence):
        # %FIXME Scaling vector 
        input_sequence = input_sequence * math.sqrt(2/3*(16-1))
        # Need to switch to vector 
        input_sequence = np.matrix.flatten(input_sequence)
        # Instantiate an empty list
        output_sequence = []
        # Decoding each element 
        for elem in input_sequence:
            # --- Real part decision 
            if np.real(elem) < -  2:
                bit1 = 1
                bit3 = 0
            elif  np.real(elem) < 0:
                bit1 = 1
                bit3 = 1
            elif  np.real(elem) < 2:
                bit1 = 0
                bit3 = 1
            else:
                bit1 = 0
                bit3 = 0

            # Imag part 
            if np.imag(elem) < -2:
                bit2 = 1
                bit4 = 0
            elif  np.imag(elem) < 0:
                bit2 = 1
                bit4 = 1
            elif  np.imag(elem) < 2:
                bit2 = 0
                bit4 = 1
            else:
                bit2 = 0
                bit4 = 0
            output_sequence.append(bit1)
            output_sequence.append(bit2)
            output_sequence.append(bit3)
            output_sequence.append(bit4)
        return output_sequence

    # Demodulation du PDSCH
    # Le msc nous dit la façon de démoduler a séquence
    # qamSeq est la séquence a démoduler
    def PDSCH_demod(self, qamSeq, mcs):
        # On a trois possibilité de modulation en fonction de mcs
        # Soit bpsk soit qpsk soir qam16
        if mcs in [5,25]:
            return self.bpsk_demod(qamSeq)
        elif mcs in [6,26]:
            return self.qpsk_demod(qamSeq)
        elif mcs in [7,27]:
            return self.qam16_demod(qamSeq)
        else:
            print("Erreur de flag mcs")
        return -1 # On renvoie -1 si la démodualtion a échoué
