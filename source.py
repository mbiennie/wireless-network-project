# Importation des lib necessaire au projet
import numpy as np  # type: ignore
import matplotlib.pyplot as plt

# Imoprtation du projet local
from Parser import Parser
from SignalManipulator import SignalManipulator
from tests_modulation import test_modulation # Class de test sur la modulation
#from matplotlib import ticker, cm 
#import pytest 
#import sk_dsp_comm.fec_conv as fec 
#import math 

# Attributs du probmème
signal_file = "tfMatrix.csv"

# ===== Définition des class du problème

# Classe pour les fonctions d'affichage : c'est une class statique
class Affichage:
    # Fonction d'affichage de graph
    # Prend en entrée une matrice pour l'afficher avec matplotlib
    @staticmethod
    def powerDistributionGraph(Z):
        fig, ax = plt.subplots()
        cs = ax.contourf(np.linspace(0, len(Z[0]), len(Z[0])), np.linspace(0,len(Z), len(Z)), np.abs(Z)**2)
        cbar = fig.colorbar(cs)
        ax.set_title('Distribution de la puissance du signal')
        ax.set_xlabel('fréquence')
        ax.set_ylabel('temps')
        plt.show()

# ===============================================
#           Résolution du sujet
# ===============================================

# On commence par instancier les class
manip_signal = SignalManipulator(signal_file)
parser = Parser(manip_signal)
test_modulation = test_modulation(parser, manip_signal)


"""
# 2.1 Extraction on the time frequency matrix
1)
La taille de la matrice est de 14*1024
On a 14 pas de temps dans une transmission
On a 1024 cannaux utilisable
Ces deux dimension nous donne les dimensions de la matrice

Il y a uniquement 624 cannaux qui sont occupés dans les faits
"""

# On affiche le signal que l'on reçoit
Affichage.powerDistributionGraph(manip_signal.signal)

"""
On peut remarques les zones suivantes :
    - Les deux premières lignes en jaune qui contiennent la syncro
    - Une grosse ligne en bleu qui représente les PSCH
    - Le haut de la matrice qui contient les données
"""

# On supprime les channels de syncro de notre signal
qamMatrix = manip_signal.remove_syncro_channels(manip_signal.signal)
# On affiche la matrice du sigan sans les channels de syncronisation
Affichage.powerDistributionGraph(qamMatrix)

"""
A ce state nous avons deux matrices disponibles : 
    - manip_signal.mat_complex : la matrice du signal sous forme complex
    - manip_signal.signal : la matrice du signal brut mais sous format réel (et non complex)
    - qamMatrix : la matrice du signal sans les cannaux de syncronisation
"""
# ===================================================

"""
# 2.2 PBCH decoding
## 2.2.1 BPSK decoding
Dans un premier temps nous allons décoder le BPSK qui est une sous partie du PBCH
1) Dans cette question, on dois montrer que l'on a bien un BPSK qui ce remarque physiquement par la caractéristique suivante :
    - Tout les parties imaginaire sont nulles
"""

# On récupère le PBSK du signal complex
PBSK = manip_signal.get_PBSK(manip_signal.mat_complex)
print("PBSK : ",PBSK)

"""
On vois bien que le PBSK ne possède pas de parties imaginaire non null
"""

"""
2) Dans cette question on dois créer une fonction qui va démoduler le sigal en un flux binaire
"""

# On traite le PBSK (en le démodulant en un flux binaire)
X = manip_signal.bpsk_demod(PBSK)

# On effectue les test unitaires fournis par le prof
test_modulation.test_bpsk()

"""
On peut voir que les test sont bien passé car le programme ne plante pas
"""

"""
3) Maintenant, on va extraire du PBSK les information sur le nombre d'user ainsi que cellindent
Pour le fichier tfMatrix.csv les information sont les suivantes
cellindent = 6174
nbuser = 40
"""
# On récupère les données du PBSK
bpsk_data = parser.extract_data_from_PBSK(PBSK)

# On affiche les données
print("Les données brut du PBSK (human readable) : ",bpsk_data)

"""
## 2.2.2 Hamming784 decoder
1) On a définie une tell fonction hamming748_decode dans la class Parser
2) On effectue les test de la class test_modulation. On peut bien voir que
notre fonction passe bien tout les tests
"""
test_modulation.test_hammingDecode()

# Traitement 
# On le décode
B = parser.hamming748_decode(X)
# On affiche l'information
print("Longueur du décodage = ",len(B))


"""
## 2.2.3 PBCH decoding
1) On va chercher les informations importatantes dans le PBCH
"""

PBCHU = [qamMatrix[0][n] for n in range(48*1,48*2)] # On attribue le PBCHU que l'on va traiter
dic_user = {}
X_user = manip_signal.bpsk_demod(PBCHU)

# On traite le PBCHU que l'on choisis
B_user = parser.hamming748_decode(X_user)
cellIdent=[]
MSC = []
symb = []
RB = []
HARQ = []
j=-1
for i in B_user:
	j=j+1
	if j<8:
		cellIdent.append(i)
	elif j>=8 and j<10:
		MSC.append(i)
	elif j>=10 and j<14:
		symb.append(i)
	elif j>=14 and j<20:
		RB.append(i)
	elif j>=20 and j<24:
		HARQ.append(i)
# On affecte les données du PBCHU dans le dico 
dic_user['userIdent'] = parser.bin2dec(cellIdent)
dic_user['MSC'] = parser.bin2dec(MSC)
dic_user['symb'] = parser.bin2dec(symb)
dic_user['RB'] = parser.bin2dec(RB)
dic_user['HARQ'] = parser.bin2dec(HARQ)


"""
# 2.3 PDCCHU decoding
## 2.3.1
"""

# On récupère le PBCH
BPCH_user_info = [qamMatrix[0][n] for n in range(0,48)]
# Trate le PBSK pour sortire des mat_complex

# On va créer un dictionnaire avec toutes les informations de la cellule

dic = parser.extract_data_from_PBSK(PBSK)


# on prends l'utilisateur 7 car MSC = 2
dic_users2 = parser.addUserDicPDCCHU(qamMatrix,dic_user['MSC'],dic_user['RB'],dic_user['symb'])
print("Le PDCCHU une fois démodulé = ",dic_users2)
# On prend un autre PBCHU

# on prends user 2 car MSC = 3
PDCCHU2 = [qamMatrix[1][n] for n in range(288,288+72)]
print(PDCCHU2)

print(parser.PDCCHU_decode(PDCCHU2,dic_user['MSC']))

# Décodage de PDCCHU en fonction du MSC
print(parser.PDCCHU_decode(PDCCHU2,1))


#========== TEST PARTIE BARBARA
dic_users2 = parser.addUserDicPDCCHU(qamMatrix,dic_user['MSC'],dic_user['RB'],dic_user['symb'])
print("dic user 2 :")
print(dic_users2)

# ===============================================================
"""
# 2.4 PDSCH decoding
## 2.4.1
1) On commence par vérifier que qam16_decode fonctionne bien
"""

test_modulation.test_qam16()

"""
On peut effectivement vérifier que la fonction fournis passe bien les tests

2) Pour cette fonction on va réutilser des fonction que l'on a codé avant
On va se baser sur le tableau qui nous donne les modulations en fonction 
"""
